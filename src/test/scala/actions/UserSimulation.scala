package actions

import java.util
import java.util.Optional
import java.util.stream.Collectors

import com.typesafe.config.Config

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer




object UserSimulation {

  val setUsers:ArrayBuffer[User] = {
    var listResult= new ArrayBuffer[User]()
    val users = ConfigTest.users
    for(index <- 0 until ConfigTest.users.size()){
      val userRaw = ConfigTest.users.get(index)
      listResult += mapTo(index,userRaw)
    }
    listResult
  }

  val takedUser:ArrayBuffer[User] = new ArrayBuffer[User]()

  {
    if(ConfigTest.enableUserGeneration){
      for(index <-ConfigTest.generateUserFrom to ConfigTest.generateUserTo){
        var userForTest:User = null;
        if(index < 10) {
          userForTest = new User(index,s"alumnoren0$index",s"alumnoren0$index","")
        }else{
          userForTest = new User(index,s"alumnoren$index",s"alumnoren${index}","")
        }
        println("generando... " + index)
        setUsers += userForTest
      }
    }
  }

  def mapTo(index:Int,userRaw:Config):User = {
    val username = userRaw.getString("user")
    val userPwd = userRaw.getString("pwd")
    val userFile = userRaw.getString("fileLoadTest")
    val userSimulation:User = new User(index,username,userPwd,userFile)
    userSimulation
  }

  def hasAvaibleUsers(): Boolean ={
    return takedUser.size<setUsers.size
  }

  def countUsers(username:String):Int = {
    setUsers.count(puser=>puser.username==username)
  }
  def getNextUser(): User = this.synchronized{
    val allowReselect:Boolean = ConfigTest.allow_reselects

    var filter:User=>Boolean = null;
    // Si permitir reselecciones esta activiado, no tomar en cuenta el numero de referencias.
    if(allowReselect){
      if(takedUser.size == setUsers.size){
        takedUser.clear()
      }
      //var personMaxNumReferences = setUsers.reduceLeft(this.findMax)
      //println("MAX REFERENCES " + personMaxNumReferences.getNumReferences())
      //filter = (person:User) =>
    }
    filter = (person:User) => !takedUser.contains(person)

    var user = setUsers.filter(filter).map(e=>Optional.ofNullable(e))
      .last

    if(!user.isPresent){
      throw  new IllegalAccessError("Restricted by configuration")
    }

    val resultUser = user.get()
    resultUser.markAsSelected()
    takedUser += resultUser
    resultUser
  }

  // returns the max of the two elements
  def findMax(x: User, y: User): User = {
    val userOne = x.getNumReferences()
    val userTwo = x.getNumReferences()

    val winner = userOne max userTwo
    x
  }
}
