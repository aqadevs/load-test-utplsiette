package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object Headers {


  val headers_0 = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Origin" -> "https://evl.grammata.es",
    "Upgrade-Insecure-Requests" -> "1")
  val headers_1 = Map("X-Requested-With" -> "XMLHttpRequest")
  val headers_3 = Map("Accept" -> "image/webp,*/*")
  val headers_12 = Map(
    "Accept" -> "application/json, text/javascript, */*; q=0.01",
    "Accept-Encoding" -> "gzip, deflate",
    "Content-Type" -> "application/json",
    "DNT" -> "1",
    "Origin" -> "http://191.232.242.116",
    "X-Requested-With" -> "XMLHttpRequest")
}
