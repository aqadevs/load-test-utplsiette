package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object ActionOpenLogin {
  val random = new Random()

  def apply() = {
    http("Siette> Abrir Siette")
      .get("/siette")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      //.body(StringBody("""username=${username}&password=${password}"""))
      .check(status.is(s => 200))
      .check(regex("Acceso"))
  }
}
