package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object ActionLogout {
  val random = new Random()

  def apply() = {
    http("Siette> logout")
      .get("/siette/generador/j_spring_security_logout")
      .header("Accept" , "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
      .header( "Origin" , "https://evl.grammata.es")
      .header(  "Upgrade-Insecure-Requests" , "1")
      .header(  "Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .check(status.is(s => 200))
  }


}
