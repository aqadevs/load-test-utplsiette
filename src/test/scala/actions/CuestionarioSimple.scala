package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object CuestionarioSimple {

  def pregunta1() = {
    http("pregunta 1")
      .post("/siette/generador/Respuesta")
      .headers(Headers.headers_1)
      .formParam("elapsedMinutes", "0")
      .formParam("elapsedSeconds", "58")
      .formParam("idpregunta0", "888")
      .formParam("idpregunta0_0", "889")
      .formParam("idrespuesta0_0", "3649")
      .formParam("hiddenSelected0_0", "3649")
      .formParam("idpregunta0_1", "622")
      .formParam("idrespuesta0_1", "2344")
      .formParam("hiddenSelected0_1", "2344")
      .formParam("idpregunta0_2", "890")
      .formParam("idrespuesta0_2", "3654")
      .formParam("hiddenSelected0_2", "3654")
      .formParam("idpregunta0_3", "891")
      .formParam("idrespuesta0_3", "3663")
      .formParam("hiddenSelected0_3", "3663")
      .formParam("idpregunta0_4", "892")
      .formParam("idrespuesta0_4", "3668")
      .formParam("hiddenSelected0_4", "3668")
      .formParam("idpregunta0_5", "623")
      .formParam("idrespuesta0_5", "2351")
      .formParam("hiddenSelected0_5", "2351")
      .formParam("idpregunta0_6", "893")
      .formParam("idrespuesta0_6", "3670")
      .formParam("hiddenSelected0_6", "3670")
      .formParam("confirmacion", "1")
      .formParam("timeStamp", "1592349732736")
      .formParam("sentido", "1")
      .formParam("finalUsuario", "0")
      .formParam("ordenPregunta", "0")
      .formParam("sigPreg", "-1")
      .formParam("tiempoRestante", "")
  }

  def pregunta2() = {
    http("pregunta 2")
      .post("/siette/generador/Respuesta")
      .headers(Headers.headers_1)
      .formParam("elapsedMinutes", "1")
      .formParam("elapsedSeconds", "7")
      .formParam("idpregunta0", "894")
      .formParam("idpregunta0_0", "895")
      .formParam("idrespuesta0_0", "3677")
      .formParam("hiddenSelected0_0", "3677")
      .formParam("idpregunta0_1", "896")
      .formParam("idrespuesta0_1", "3680")
      .formParam("hiddenSelected0_1", "3680")
      .formParam("idpregunta0_2", "897")
      .formParam("idrespuesta0_2", "3685")
      .formParam("hiddenSelected0_2", "3685")
      .formParam("confirmacion", "1")
      .formParam("timeStamp", "1592349793235")
      .formParam("sentido", "1")
      .formParam("finalUsuario", "0")
      .formParam("ordenPregunta", "1")
      .formParam("sigPreg", "-1")
      .formParam("tiempoRestante", "")
  }

  def pregunta3() = {
    http("pregunta 3")
      .post("/siette/generador/Respuesta")
      .headers(Headers.headers_1)
      .formParam("elapsedMinutes", "1")
      .formParam("elapsedSeconds", "23")
      .formParam("idpregunta0", "886")
      .formParam("idpregunta0_0", "887")
      .formParam("idrespuesta0_0", "3647")
      .formParam("hiddenSelected0_0", "3647")
      .formParam("idpregunta0_1", "898")
      .formParam("idrespuesta0_1", "3694")
      .formParam("hiddenSelected0_1", "3694")
      .formParam("idpregunta0_2", "899")
      .formParam("idrespuesta0_2", "3699")
      .formParam("hiddenSelected0_2", "3699")
      .formParam("idpregunta0_3", "900")
      .formParam("idrespuesta0_3", "3703")
      .formParam("hiddenSelected0_3", "3703")
      .formParam("idpregunta0_4", "901")
      .formParam("idrespuesta0_4", "3708")
      .formParam("hiddenSelected0_4", "3708")
      .formParam("confirmacion", "1")
      .formParam("timeStamp", "1592349807371")
      .formParam("sentido", "1")
      .formParam("finalUsuario", "0")
      .formParam("ordenPregunta", "2")
      .formParam("sigPreg", "-1")
      .formParam("tiempoRestante", "")
  }

  def enviar() = {
    http("Enviar")
      .post("/siette/generador/Respuesta")
  }
}
