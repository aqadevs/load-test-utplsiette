package actions

import com.typesafe.config.ConfigFactory

object ConfigTest {

  val config = ConfigFactory.load("simulation").getConfig("local")

  val onlyDefault  =  config.getString("onlyDefault")

  val baseUrl = config.getString("baseUrl")

  val users = config.getConfigList("users")

  // permite seleccionar un usuario nuevamente para pruebas en un estilo circular
  val allow_reselects:Boolean = config.getBoolean("allow_reselects")


  val modalidades = config.getString("modalidad")

  val peekUsers = config.getInt("peek_users")

  val enableUserGeneration = config.getBoolean("generate_users")
  val generateUserFrom = config.getInt("generate_user_from")
  val generateUserTo = config.getInt("generate_user_to")
}
