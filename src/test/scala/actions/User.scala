package actions

import java.util.concurrent.atomic.AtomicInteger

import io.gatling.core.session.Session

/**
 * User, represeta un usuario de pruebas
 *
 * @param username
 * @param password
 * @param fileTest
 */
class User(
  val userId:Int,
  val username: String,
  val password: String,
  val fileTest: String,
) {

  private var selected: Boolean = false;
  private var atomicInteger: AtomicInteger = new AtomicInteger(0)
  private var fileSectCode:String = "";
  private var filePeriodo:String = ""

  {
    if(filePeriodo != null && !filePeriodo.isEmpty){
      // Calificaciones-40007-201964
      val sectCodeCalcArray = this.fileTest.split("-").toArray[String];
      if(sectCodeCalcArray.size>=2){
        // cosa jodidamente mal programa por lo loque no hay timaok
        this.fileSectCode = sectCodeCalcArray(1);
        this.filePeriodo = sectCodeCalcArray(2).replace(".xlsm","");
      }
    }


  }
  /**
   * markAsSelected, marca a este usuario como seleccionado en la bateria de pruebas,
   * la restricción es no tomar un usuario repetido para poder hacer seguimiento en la
   * bateria de pruebas
   */
  def markAsSelected(): Unit = {
    atomicInteger.incrementAndGet()
    this.selected = true
  }

  def getNumReferences(): Int = {
    this.atomicInteger.get()
  }

  /**
   * returns boolean
   *
   * @return
   */
  def isSelected(): Boolean = {
    return this.selected;
  }


  /**
   * get Current user as map
   * @return
   */
  def getAsMap():Map[String,Any]={
    Map(
      "username"->this.username,
      "password"->this.password,
      //"file"->this.fileTest,
      "fileSectCode"->this.fileSectCode,
      "filePeriodo"->this.filePeriodo,
      "references"->this.getNumReferences()
    )
  }

  def toSession(session:Session):Session={
    var m = session;
    m = m.set("username",username)
    m = m.set("password",password)
    m = m.set("fileSectCode",fileSectCode)
    m = m.set("filePeriodo",filePeriodo)
    m = m.set("references",getNumReferences())
    m
  }

}