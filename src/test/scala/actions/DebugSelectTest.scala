package actions



import io.gatling.core.Predef._

object DebugSelectTest {


  def apply(session: Session):Session = {
    val selectedTest:String = session("url_test").as[String]
    println("============================ TEST URL SELECTED =========================")
    println("Selected test is " + selectedTest)
    println("========================================================================")
    return session
  }
}
