package actions



import io.gatling.core.Predef._

object DebugSession {


  def apply(session: Session):Session = {
    val currentUsername:String = session("username").as[String]
    val currentNumReferences:String = session("references").as[String]
    //println("============================ CONTEXT RESULT USER SELECTION =========================")
    UserSimulation.setUsers.foreach((user)=>{
      //println(s"USER [${user.username}] REFERENCES [${user.getNumReferences()}]")
    })
    //println("========================================================================")
    return session
  }
}
