package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object ActionConfirmTest {
  val random = new Random()
  val headers = Map(
    "Origin" -> "https://evl.grammata.es",
    "Upgrade-Insecure-Requests" -> "1",
    "Accept-Language"->"es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3"
  )

  def apply() = {
    http("Siette> Confirmar selección de test ${name_test}")
      .post("/siette/generador/Pregunta")
      .headers(headers)
      .formParam("confirmacion", "true")
  }
}
