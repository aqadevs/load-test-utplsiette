package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object ActionNextTest {
  val random = new Random()

  def openNextTest3() = {
    http("Siette> Mis Next TEST")
      .get("/siette/my-next-tests")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .check(status.is(s => 200))
      .check(css("#test_7486 > td:nth-child(2) > a:nth-child(2)", "href").saveAs("url_test"))
      .check(css("#test_7486 > td:nth-child(2) > a:nth-child(2)").saveAs("name_test"))
  }


  def openNextTest15() = {
    http("Siette> Mis Next TEST")
      .get("/siette/my-next-tests")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .check(status.is(s => 200))
      .check(css("#test_7488 > td:nth-child(2) > a:nth-child(2)", "href").saveAs("url_test"))
      .check(css("#test_7488 > td:nth-child(2) > a:nth-child(2)").saveAs("name_test"))
  }

  def openNextTest15A() = {
    http("Siette> Mis Next TEST")
      .get("/siette/my-next-tests")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .check(status.is(s => 200))
      .check(css("#test_7488 > td:nth-child(2) > a:nth-child(2)", "href").saveAs("url_test"))
      .check(css("#test_7488 > td:nth-child(2) > a:nth-child(2)").saveAs("name_test"))
  }

  def openNextTestConcurreciaSinImagenes() = {
    http("Siette> Mis Next TEST")
      .get("/siette/my-next-tests")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .check(status.is(s => 200))
      .check(css("#test_7493 > td:nth-child(2) > a:nth-child(2)", "href").saveAs("url_test"))
      .check(css("#test_7493 > td:nth-child(2) > a:nth-child(2)").saveAs("name_test"))
  }

  def openNextTest(id_test: Int) = {
    http("Siette> Mis Next TEST")
      .get("/siette/my-next-tests")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .check(status.is(s => 200))
      .check(css("#test_" + id_test +" > td:nth-child(2) > a:nth-child(2)", "href").saveAs("url_test"))
      .check(css("#test_" + id_test +" > td:nth-child(2) > a:nth-child(2)").saveAs("name_test"))
  }


}