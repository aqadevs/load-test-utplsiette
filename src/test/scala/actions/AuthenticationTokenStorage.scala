package actions

import java.io.{File, FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import io.gatling.core.Predef.Session

import scala.collection.mutable

/**
 * TokenStorage, esta clase es serializable, se escribe y se actualiza en disco bajo el nombre
 * de FILE_NAME_STORAGE, esta clase mantiene un mapa con los tokens asociados a los usuarios bajo
 * test
 */
@SerialVersionUID(2234L)
class AuthenticationTokenStorage extends Serializable {

  private val tokenMaps: mutable.HashMap[String, AuthenticationRawToken] = new mutable.HashMap[String, AuthenticationRawToken]()


  /**
   * saveToken, guarda el token en cache
   *
   * @param token
   */
  def saveToken(token: AuthenticationRawToken): Unit = synchronized {
    val username = token.username
    tokenMaps.put(username, token)
  }

  /**
   * getTokenForUser,retorna el token para un usuario
   *
   * @param username
   * @return
   */
  def getTokenForUser(username: String): Option[AuthenticationRawToken] = {
    tokenMaps.get(username)
  }

  /**
   * hasToken, metodo retorna bool indicando si tiene en cache un token para un usuarioi
   * @param username
   * @return
   */
  def hasToken(username: String): Boolean = {
    tokenMaps.contains(username)
  }

  /**
   * filter determina sitiene tokens invalidos en cache
   * @return
   */
  def hasInvalidToken(): Boolean = {
    val filter = (token: AuthenticationRawToken) => token.isTokenExpired()
    val resultHasInvalids = tokenMaps.values.filter(filter)
    resultHasInvalids.nonEmpty
  }


}

/**
 * Static methods, using singleton compatiopn object
 */
object AuthenticationTokenStorage {
  private val FILE_NAME_STORAGE = s"TOKENS_SERIAL_CACHE";
  var stokenStorage: AuthenticationTokenStorage = null

  /**
   * Read all tokens from file store, if exists read into object serializable
   * @return
   */
  def loadInstance(): AuthenticationTokenStorage = synchronized {
    if (stokenStorage == null) {
      val file = new File(FILE_NAME_STORAGE)
      if (!file.exists()) {
        stokenStorage = new AuthenticationTokenStorage()
      } else {
        val ois = new ObjectInputStream(new FileInputStream(file))
        stokenStorage = ois.readObject.asInstanceOf[AuthenticationTokenStorage]
        ois.close()
      }
    }
    stokenStorage
  }

  /**
   * serializeAndSave, write all tokens into serializabvle objectass
   * @param tokenStorage
   */
  def serializeAndSave(tokenStorage: AuthenticationTokenStorage): Unit  = synchronized {
    // (2) write the instance out to a file
    val oos = new ObjectOutputStream(new FileOutputStream(FILE_NAME_STORAGE))
    oos.writeObject(tokenStorage)
    oos.close()
  }

  /**
   * Write cache in disk
   * @param session
   * @return
   */
  def saveInCache(session: Session):Session = {
    val instance = loadInstance()
    val currentUsername:String = session("username").as[String]
    val token = session("token").as[String]
    val expireInSeconds:Long = session("expireInSeconds").as[Long]
    val rawToken = new AuthenticationRawToken(currentUsername,token,expireInSeconds)
    instance.saveToken(rawToken)
    AuthenticationTokenStorage.serializeAndSave(instance)
    session
  }
//
//  /**
//   * CloadFromCache
//   * @param session
//   * @return
//   */
//  def loadFromCache(session: Session):Session = {
//    val instance = loadInstance()
//    val currentUsername:String = session("username").as[String]
//    val token = instance.getTokenForUser(currentUsername)
//    session.set("token",token.get.accessToken)
//    session
//  }
}
