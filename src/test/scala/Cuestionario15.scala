import io.gatling.core.Predef._
import io.gatling.http.Predef._
import actions._
/**
 * Auth, esta simulacíón es usada para incializar todos los tokens de un usuario antes de ejecutar las pruebas de carga,
 * una vez ejecutada esta simulacíón todos los tokens seran almacenados e indexados para cada usuario, para que este
 * proceso no afecte el test de carga de archivos.
 *
 * Se intenta Obtener un token con 3 reintentos maximos por docente.
 */
class Cuestionario15 extends Simulation {
  /**
   * ======================= HTTP PROTOCOL GLOBAL CONFIG=====================
   **/
  val httpProtocol = http
    .baseUrl(ConfigTest.baseUrl)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .doNotTrackHeader("1")
    .inferHtmlResources() // simula el comportamiento del navegador al descargar todo los assets img, videos etc...
    .maxConnectionsPerHostLikeChrome
    .silentResources
    .disableWarmUp
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
    .maxRedirects(3)
    .disableCaching


  val selectUser = Iterator.continually(UserSimulation.getNextUser().getAsMap())
  val authFlow = scenario("Auth Flow")
    .exec(feed(selectUser))
    .exec(sessionFunction = (session) => DebugSession(session))
    .exec(ActionOpenLogin())
    .exec(AuthenticationAction())
    .exec(ActionNextTest.openNextTest15A())
    .exec(sessionFunction = (session) => DebugSelectTest(session))
    .exec(ActionOpenTest())
    .exec(ActionConfirmTest())
    .exec(_.set("unixTime", System.currentTimeMillis / 1000))
    .exec(CuestionarioComplejo.resolve(1))
    .exec(_.set("unixTime", System.currentTimeMillis / 1000))
    .exec(CuestionarioComplejo.resolve(2))
    .exec(_.set("unixTime", System.currentTimeMillis / 1000))
    .exec(CuestionarioComplejo.resolve(3))
    .exec(_.set("unixTime", System.currentTimeMillis / 1000))
    .exec(CuestionarioComplejo.resolve(4))
    .exec(_.set("unixTime", System.currentTimeMillis / 1000))
    .exec(CuestionarioComplejo.resolve(5))
    //.exec(http("Tick")
    //  .get("/siette/realized-test-tick?realizedTestId=1461727"))
    .exec(ActionLogout())
    //.exec(CuestionarioComplejo.resolve(15))
    //.exec(CuestionarioComplejo.resolve(16))
    //.exec(CuestionarioComplejo.enviar())


    //.exec(CuestionarioComplejo.enviar())


  setUp(
    authFlow.inject(atOnceUsers(1)),
  ).protocols(httpProtocol)
}

